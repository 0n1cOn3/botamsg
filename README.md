# BotaMsg

A script that helps you to send message with your telegram bot to groups in telegram

## Steps to use this script

### Create a bot & get bot token from https://t.me/botfather . (Example Here: https://youtu.be/MZixi8oIdaA) 

### Next add your bot in the group(s) of your likings.

### Next get chat Id of the group(s) by typing /id . (That group should have a group management bot which has /id command available to use.)

### Copy the bot token and chat Id(s) and use in this script.
