#!/usr/bin/bash

Setup() {

clear
    
read -p $'\n\033[3;96mEnter your bot token: \033[0m' token

curl -X POST https://api.telegram.org/bot$token/getme &>.test
testtoken=$(cat .test | grep ok | cut -c 7-10)
case $testtoken in
    true)
	;;
    
    fals)
	echo -e "\033[91mInvalid token!!!"
	sleep 2
	clear
	Setup
	;;
    
esac
    
read -p $'\n\033[3;92mHow many groups do want to add?(1/2): \033[0m' groups
touch .profile_plugins
echo $groups > .profile_plugins
case $groups in
    1)
	read -p $'\n\033[2;95mEnter chat id: \033[0m' chatid
	read -p $'\n\033[2;95mEnter nickname of group: \033[0m' name
	echo $name >> .profile_plugins
	curl -X POST https://api.telegram.org/bot$token/sendmessage -d text=test -d chat_id=$chatid &>.chtid
	testchatid=$(cat .chtid | grep ok | cut -c 7-10)
	case $testchatid in
	    true)

		;;
	    fals)
		echo ""
		echo -e "\033[91mInvalid chat id!!!"
		sleep 2
		clear
		Setup
		;;
	esac
	echo Chatid:"$chatid" >> plugins.installed
	;;

    2)
	read -p $'\n\033[3;94mEnter chat id: \033[0m' chatid1
 	read -p $'\n\033[3;96mEnter second chat id: \033[0m' chatid2
        curl -X POST https://api.telegram.org/bot$token/sendmessage -d text=test -d chat_id=$chatid1 &>.chtid1
	curl -X POST https://api.telegram.org/bot$token/sendmessage -d text=test -d chat_id=$chatid2 &>.chtid2
	testchatid1=$(cat .chtid1 | grep ok | cut -c 7-10)
	testchatid2=$(cat .chtid2 | grep ok | cut -c 7-10)
	case $testchatid1 in
	    true)
		
		read -p $'\n\033[3;94mEnter nickname of first group: \033[0m' name1
		;;
	    fals)
		echo ""
		echo -e "\033[91mInvalid chat id!!!"
		sleep 2
		clear
		Setup
		;;
	esac
	case $testchatid2 in
	    true)
		read -p $'\n\033[3;92mEnter nickname of second group: \033[0m' name2
		;;
	    fals)
		echo ""
		echo -e "\033[91mInvalid chat id!!!"
		sleep 2
		clear
		Setup
		;;
	esac
	echo $name1 >> .profile_plugins
	echo $name2 >> .profile_plugins
	echo Chatid1:"$chatid1" >> plugins.installed
	echo Chatid2:"$chatid2" >> plugins.installed
	;;
    
    *)
	echo -e "\033[3;91mIncorrect option!!!"
	Setup
	;;
    
        
esac




echo Token:$token >> plugins.installed

}

Banner() {

clear
echo -e "\033[36m=======================================\033[0m"
echo '''
 ____        _        __  __           
| __ )  ___ | |_ __ _|  \/  |___  __ _ 
|  _ \ / _ \| __/ _` | |\/| / __|/ _` |
| |_) | (_) | || (_| | |  | \__ \ (_| |
|____/ \___/ \__\__,_|_|  |_|___/\__, |
                                 |___/ 
'''
echo -e "\033[36m=======================================\033[0m"
echo -e "\033[92m                                   V1.5\033[0m"
echo ""
echo -e "\033[3;96m - A cli-based program that hacks nasa jk lol. It is a cli-based program that can help you send message to telegram groups with a bot.\033[0m" 
echo ""
}

Main-Stuff() {

echo -e "\033[95m1. Send message to groups\033[0m"
echo ""
echo -e "\033[95m2. Reset parameters\033[0m"
echo ""
echo -e "\033[95m3. Update\033[0m"
echo ""
echo -e "\033[95m4. Exit\033[0m"
echo ""
read -p $'\n\033[96mChoose an option: ' choice

case $choice in
    1)
	
	
    
groups=$(head -n 1 .profile_plugins)
name=$(awk -F: 'NR ==2' .profile_plugins)
name1=$(awk -F: 'NR ==2' .profile_plugins)
name2=$(awk -F: 'NR ==3' .profile_plugins)
case $groups in
    1)
	
	read -p $'\n\e[3;96mEnter Your Message: \e[0m' msg
	token=$(cat plugins.installed | grep "$Token" | cut -c7- | awk -F: 'NR ==2')
	chatid=$(cat plugins.installed | grep "$Chatid" | cut -c8- | head -n1)
	echo ""
	echo $name:$msg >> messages.txt
	curl -X POST "https://api.telegram.org/bot$token/sendmessage" -d "chat_id=$chatid" -d "text=$msg" &>.msg
	echo ""
	echo Message sent:$(cat .msg | grep true | head -n1 | cut -c 7-10)
	echo ""
	Main-Stuff
       	;;
    2)
	echo ""
	echo "1. $name1"
	echo ""
	echo "2. $name2"
	echo ""
	read -p $'\n\e[3;96mChoose the chat you wanna send message to: \e[0m' chat
	;;
esac

case $chat in
    1)
	read -p $'\n\e[3;96mEnter Your Message: \e[0m' msg
	token=$(cat plugins.installed | grep "$Token" | cut -c7- | awk -F: 'NR ==3')
	chatid1=$(cat plugins.installed | grep "$Chatid1" | cut -c9- | head -n1)
	echo ""
	echo $name1:$msg >> messages.txt
	curl -X POST "https://api.telegram.org/bot$token/sendmessage" -d "chat_id=$chatid1" -d "text=$msg" &>.msg
	echo ""
	echo Message sent:$(cat .msg | grep ok | head -n1 | cut -c 7-10)
	echo ""
	Main-Stuff
       	;;
    
    2)
	read -p $'\n\e[3;96mEnter your message: \e[0m' msg
	chatid2=$(cat plugins.installed | grep "$Chatid2" | cut -c9- | awk -F: 'NR ==2')
	echo ""
	echo $name2:$msg >> messages.txt
	curl -X POST "https://api.telegram.org/bot$token/sendmessage" -d "chat_id=$chatid2" -d "text=$msg" &>.msg
	echo ""
	echo Message sent:$(cat .msg | grep ok | head -n1 | cut -c 7-10)
	echo ""
	Main-Stuff
       	;;

    *)
	echo -e "\033[91mIncorrect option!!!"
	Banner
	Main-Stuff
	;;
esac
	;;
    
    2)
	clear
	echo -e "\033[91mRemoving all parameters....\033[0m"
	rm .chtid* messages.txt .msg plugins.installed .test .profile_plugins
	echo ""
	echo -e "\033[92mExiting script.....\033[0m"
	sleep 2
	exit 0
	;;

    3)
	clear
	echo -e "\033[96mUpdating...."
	echo ""
	git pull
	echo ""
	echo -e "\033[96mUpdated!\033[0m"
	sleep 2
	clear
	Banner
	Main-Stuff
	;;

    4)
	clear
	echo -e "\033[96mExiting......\033[0m"
	echo ""
	base64 -d <<<"ICBfX19fX19fIF8gICAgICAgICAgICAgICAgIF8gICAgICAgICAgIF9fICAgICAgICAgICAgICAg
ICAgICAgICBfICAgICAgICAgICAgICAgICAgICAgCiB8X18gICBfX3wgfCAgICAgICAgICAgICAg
IHwgfCAgICAgICAgIC8gX3wgICAgICAgICAgICAgICAgICAgICAoXykgICAgICAgICAgICAgICAg
ICAgIAogICAgfCB8ICB8IHxfXyAgIF9fIF8gXyBfXyB8IHwgX19fX18gIHwgfF8gX19fICBfIF9f
ICAgXyAgIF8gX19fIF8gXyBfXyAgIF9fIF8gICAgICAgICAKICAgIHwgfCAgfCAnXyBcIC8gX2Ag
fCAnXyBcfCB8LyAvIF9ffCB8ICBfLyBfIFx8ICdfX3wgfCB8IHwgLyBfX3wgfCAnXyBcIC8gX2Ag
fCAgICAgICAgCiAgICB8IHwgIHwgfCB8IHwgKF98IHwgfCB8IHwgICA8XF9fIFwgfCB8fCAoXykg
fCB8ICAgIHwgfF98IFxfXyBcIHwgfCB8IHwgKF98IHxfIF8gXyBfIAogICAgfF98ICB8X3wgfF98
XF9fLF98X3wgfF98X3xcX1xfX18vIHxffCBcX19fL3xffCAgICAgXF9fLF98X19fL198X3wgfF98
XF9fLCAoX3xffF98XykKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfXy8gfCAgICAgICAgCiAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICB8X19fLyAgICAgICAgIAoK" | lolcat
	exit 0
	;;
esac
}


if [ -f plugins.installed ] ; then

Banner
Main-Stuff

else
	Setup
	Banner
	Main-Stuff

fi
